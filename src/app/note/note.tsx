import Link from "next/link";

export default function Note({ note }: any) {
  const { id, title, content, created } = note || {};

  return (
    <Link href={`/notes/${id}`}>
      <div>
        <h2>{title}</h2>
        <h3>{content}</h3>
        <span>{created}</span>
      </div>
    </Link>
  );
}
