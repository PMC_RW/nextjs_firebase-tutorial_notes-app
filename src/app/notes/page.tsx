// import PocketBase from "pocketbase";
import CreateNote from "./CreateNote";
import Note from "../note/note";

// export const dynamic = "auto",
//   dynamicParams = true,
//   revalidate = 0,
//   fetchCache = "auto",
//   runtime = "nodejs",
//   preferredRegion = "auto";

async function getNotes() {
  // const pb = new PocketBase("http://127.0.0.1:8090");
  // const data = await pb.collection("notes").getList(1, 50, {
  //   sort: "created",
  // });

  const res = await fetch(
    "http://127.0.0.1:8090/api/collections/notes/records",
    { cache: "no-store" }
  );
  const data = await res.json();
  return data?.items as any[];
}

export default async function NotesPate() {
  const notes = await getNotes();

  return (
    <div>
      <h1>Notes</h1>
      <div>
        {notes?.map((note) => {
          return <Note key={note.id} note={note} />;
        })}
      </div>
      <CreateNote />
    </div>
  );
}
