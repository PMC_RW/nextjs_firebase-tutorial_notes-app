import PocketBase from "pocketbase";

async function getNote(noteId: string) {
  const pb = new PocketBase("http://127.0.0.1:8090");
  const data = await pb.collection("notes").getOne(noteId, {
    expand: "relField1,relField2.subRelField",
  });
  return data;
}

export default async function NotePage({ params }: any) {
  const note = await getNote(params.id);

  return (
    <div>
      <h1>{note.title}</h1>
      <p>{note.content}</p>
      <span>{note.created}</span>
    </div>
  );
}
